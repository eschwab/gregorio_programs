# Gregorio Programs

Here are some helper programs for creating/modifying GABC files for Gregorio.

## Files
---------

### DictionarySort.py
Quick little program to resort the dictionary file. Additions made by syllabate.py simply append the new keywords, this program returns the dictionary to proper alphabetical order.

Usage:

    :::sh
    DictionarySort.py

### english_dictionary
Required by syllabate.py. Provides a list of key,value pairs.

### syllabate.py
Program for splitting up words into syllables with ()'s. Currently only for english. Depends upon the provided dictionary file.

Usage:

    :::sh
    syllabate.py -i <inputfile>

### transcribe.py
Program for transcribing gabc files. For either moving the clef up or down.

Usage:

    :::sh
    transcribe.py -i <inputfile> -d <up|down>
