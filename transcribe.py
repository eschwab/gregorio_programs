#! /usr/bin/env python

import re
import os
import sys
import getopt


def sourcescore(sourcefile):
    with open(sourcefile, 'r') as rawsrc:
        gabcsrc = rawsrc.read()
    return gabcsrc

def definitions(sysarg):
    """Sets source file and direction of transposition from either command
    line arguments, or user input.

    """
    filename = ''
    direction = ''
    try:
        opts, args = getopt.getopt(sysarg, 'hi:d:')
    except getopt.GetoptError:
        print('Usage: transcribe.py -i <inputfile> -d <up|down>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: transcribe.py -i <inputfile> -d <up|down>')
            sys.exit(2)
        elif opt in '-i':
            filename = arg
        elif opt in '-d':
            direction = arg
    if filename == '':
        filename = input('Which file to transpose? ')
    while direction == '':
        print('Select a direction to transpose:')
        print('1. up')
        print('2. down')
        direction = input('>')
        print()
    return (filename, direction)

def rangetest(notes):
    """Tests to see if the source score may exceed the limits of the staff
    after transposition.

    """
    if direction == '1' or direction == 'up':
        if re.match(r'.*[l-mL-M]+.*', notes):
            sys.exit('Score is too high. Cannot transpose.')
        else:
            return
    elif direction == '2' or direction == 'down':
        if re.match(r'.*[a-bA-B]+.*', notes):
            sys.exit('Score is too low. Cannot transpose.')
        else:
            return

def transup(parens):
    """Raises the clef on the staff and moves the notes accordingly."""
    rangetest(parens)
    if re.match(r'\([c|f]?[2-3]?\)', parens):
        clef = re.sub(r'[2-3]', lambda x: chr(ord(x.group()) +1), parens)
        return clef
    else:
        result = re.sub(r'[a-kA-K]', lambda x: chr(
                 ord(x.group(0)) +2), parens)
        return result

def transdown(parens):
    """Lowers cleff and notes."""
    rangetest(parens)
    if re.match(r'\([c|f]?[3-4]?\)', parens):
        clef = re.sub(r'[3-4]', lambda x: chr(ord(x.group()) -1), parens)
        return clef
    else:
        result = re.sub(r'[c-mC-M]', lambda x: chr(
                 ord(x.group(0)) -2), parens)
        return result

def replacenotes(source):
    """The replace value for the re.sub in gabcout."""
    if direction == '1' or direction == 'up':
        result = transup(source.group())
        return result
    elif direction == '2' or direction == 'down':
        result = transdown(source.group())
        return result

def writescore():
    """Make backup, write new score."""
    os.rename(filename, filename[:-5] + 'OLD' + filename[-5:])
    oldfile = filename[:-5] + 'OLD' + filename[-5:]
    with open(filename, 'w') as file:
        file.write(gabcout)
    print('Moved original file to: ' + oldfile)
    print('Wrote new gabc file: ' + filename)


if __name__ == '__main__':
    filename, direction = definitions(sys.argv[1:])
    gabcsrc = sourcescore(filename)
    gabcout = re.sub(r'\([^)]+\)', replacenotes, gabcsrc)
    writescore()
