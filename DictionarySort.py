#! /usr/bin/env python

dictionaryfile = 'english_dictionary'

englishdic = {}

with open(dictionaryfile, 'r') as dicfile:
    for line in dicfile:
#        print(line)
        (key, val) = line.split()
        englishdic[key] = val

with open(dictionaryfile, 'a') as f:
    for key in sorted(englishdic.keys()):
        line = key + ' ' + englishdic[key]
#        print(line)
        f.write(line + '\n')
