#! /usr/bin/env python

import re
import os
import sys
import getopt


englishdicfile = 'english_dictionary'
englishdic = {}
result = []


def arguments(sysarg):
    """
    Define source gabc file. From either command line argument or by input.

    """
    filename = ''
    try:
        opts, args = getopt.getopt(sysarg, 'hi:')
    except getopt.GetoptError:
        print('Usage: syllabate.py -i <inputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: syllabate.py -i <inputfile>')
            sys.exit(2)
        elif opt in '-i':
            filename = arg
    if filename == '':
        filename = input('Which file to syllabate?\n>')
        print('')
    return (filename)

def read_englishdic_in():
    """Read the dictionary file into englishdic."""
    with open(englishdicfile, 'r') as dicfile:
        for line in dicfile:
            (key, val) = line.split()
            englishdic[key] = val

def scoreinput(path):
    """Reads the source file into a list making each line a list item."""
    with open(path, 'r') as scorefile:
        filein = scorefile.readlines()
    return filein

def adddicentry(key):
    """The user wants to add the missing word to the dictionary.
    Ask for a syllabated value. Then append the new key,value pair to
    englishdic, the output file, and englishdicfile.
    
    """
    print('Enter the syllabic form of ' + key.upper())
    newvalue = input('>')
    englishdic[key] = newvalue.capitalize()
    result.append(newvalue)
    with open(englishdicfile, 'a') as dicfile:
        dicfile.write(key.upper() + ' ' + newvalue.capitalize() + '\n')

def noentryindic(key):
    """If a word is not in the dictionary, ask the user if they want to add
    the word with it syllabic form to the dictionary.

    """
    answer = input(key + ' is not in the dictionary. ' +
                   'Would you like to add it? \n1. yes\n2. no\n>')
    if re.match(r'1|y[es]?', answer):
        adddicentry(key)
    else:
        print('Skipping word.\n')
        result.append(key)

def replace(line):
    """Runs through each word in the lines fed by parselines().
    Makes the following tests:
    > word already in syllabic form.
    > ending punctuation, i.e. .:;?
    > capitalization
    > dictionary entry for the word.

    Replaces the word with the appropriate modifiers. Adds the final word-form
    to 'result'. Adds a newline before moving to the next line.

    """
    line = line.split()
    for wordin in line:
        addending = ''
        caps = ''
        if re.search(r'\(.*\)', wordin):
            result.append(wordin)
            continue
        if re.match(r'.*[,|;|:|\.|\?]+', wordin):
            ending = wordin[-1]
            wordin = wordin[:-1]
            addending = '1'
        if wordin == wordin.capitalize():
            caps = '1'
        word = wordin.upper()
        if word in englishdic:
            match = englishdic[word].lower()
            if addending == '1' and caps == '1':
                result.append(match.capitalize()[:-2] + ending + '()')
            elif addending == '1':
                result.append(match[:-2] + ending + '()')
            elif caps == '1':
                result.append(match.capitalize())
            else:
                result.append(match)
        else:
            noentryindic(wordin)
    result.append('\n')

def parse_lines(source):
    """Reads each list item of the source file. Tests for:
    > header lines
    > comment lines
    > end of header line - '%%'

    """
    for line in source:
        if re.match(r'%', line) or re.match(r'.*:.*;$', line):
            result.append(line)
        else:
            replace(line)

def writeout(filename):
    """Overwriets the source file with the new goodness."""
    with open(filename, 'w') as goal:
        goal.write(re.sub(r'\n ', r'\n', ' '.join(result)))

def main():
    filetosyllabate = arguments(sys.argv[1:])
    source = scoreinput(filetosyllabate)
    read_englishdic_in()
    parse_lines(source)
    writeout(filetosyllabate)
    ## For testing:
    ## Comment out previous line (writeout) and uncomment the following.
#    print(re.sub(r'\n ', r'\n', ' '.join(result)))
    print('\nSucessfully completed operation')


if __name__ == '__main__':
    main()
